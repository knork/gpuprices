/**
 * Created by knork on 8/2/17.
 */


google.charts.load('current', {'packages': ['corechart']});
google.charts.load('current', {'packages':['annotationchart']});

google.charts.setOnLoadCallback(charts);

var filePath;

function loadJSON(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    // prevent caching
    // var uniq = new Date().getTime() / (1000*60*60); // caching for up to one hour
    xobj.open('GET', filePath, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send();
}


function charts(){

    var idElem= document.getElementById('chart');
    if(!idElem){

        var elements = document.getElementsByClassName('chart');
        var i;
        for (i = 0; i < elements.length; i++) {
            filePath = elements[i].getAttribute("file");
            drawChart( elements[i]);
        }
    }
    else {
        filePath = idElem.getAttribute("file");
        drawChart(idElem);
    }

}

function drawChart(elem) {

    loadJSON(function (response) {
        response = JSON.parse(response);
        var data = new google.visualization.DataTable(response);
        var chart = new google.visualization.AnnotationChart(elem);
        var maxVal = data.getColumnRange(1).max;
        var options = {
            displayAnnotations: false,
            min: 0,
            max: maxVal+(maxVal/7),
            colors: ['#000a73', '#880606', '#00b300']
        };
        chart.hideDataColumns(1);
        chart.draw(data, options);
    });
}

