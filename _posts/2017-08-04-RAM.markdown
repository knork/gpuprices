---
layout: post
title:  "RAM"
date:   2017-08-02 22:26:41 +0200
categories: ram overview
---

All prices are in €(EUR), data is fetched from [geizhals](geizhals.de)

### DDR 3 8GB

<div class="chart" file="/gpuprices/res/DDR3_8GB.json" style="width: 900px; height: 500px"></div>
<br>

### DDR 3 16GB

<div class="chart" file="/gpuprices/res/DDR3_16GB.json" style="width: 900px; height: 500px"></div>
<br>


### DDR 3 32GB

<div class="chart" file="/gpuprices/res/DDR3_32GB.json" style="width: 900px; height: 500px"></div>
<br>


### DDR 3 64GB

<div class="chart" file="/gpuprices/res/DDR3_64GB.json" style="width: 900px; height: 500px"></div>
<br>


### DDR 4 8GB

<div class="chart" file="/gpuprices/res/DDR4_8GB.json" style="width: 900px; height: 500px"></div>
<br>

### DDR 4 16GB

<div class="chart" file="/gpuprices/res/DDR4_16GB.json" style="width: 900px; height: 500px"></div>
<br>
### DDR 4 32GB

<div class="chart" file="/gpuprices/res/DDR4_32GB.json" style="width: 900px; height: 500px"></div>
<br>
### DDR 4 64GB

<div class="chart" file="/gpuprices/res/DDR4_64GB.json" style="width: 900px; height: 500px"></div>
<br>
### DDR 4 128GB

<div class="chart" file="/gpuprices/res/DDR4_128GB.json" style="width: 900px; height: 500px"></div>
<br>


