
import fileinput
import re
import glob, os

os.chdir(".")
# file = "amd_rx_580.json";

for file in glob.glob("*.json"):
    newLines = [];
    f = open(file);
    for line in f:
        line = re.sub(r'(2017,)(\d{1,2})', lambda m: str("2017,"+str((int(m.group(2))-1))), line.rstrip())
        newLines.append(line+'\n');
        # print(line)
    f.close();
    w = open(file, 'w')
    w.writelines(newLines);
    w.close();
# os.chdir(".")
# for file in glob.glob("*.json"):
#     print(file)
#     readFile = open(file)
#     lines = readFile.readlines()
#     readFile.close()
#     for line in lines:
#         print(line)