import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup
import os, sys
import datetime
from datetime import timedelta
import re
import time


def getPrices( wiki, file):
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.12')]

    page = opener.open(wiki)
    soup = BeautifulSoup(page,"lxml")

    all_prices = soup.find_all("span",{"class" : "gh_price"})
    length = 0
    total = 0
    min =100000
    max = 0
    for price in all_prices:
        price_string = price.string

        if(price_string):
            price_string=price_string.replace(',', '.')
            price_string = price_string.replace('--', '')
            length+=1

            val = float(price_string.split()[1])
            total += val
            if(min> val):
                min = val
            if (max < val):
                max = val


    # print "{} : {}".format("length", length)
    # print "{} : {}".format("total", total)
    # if(length!=0):
    #     print "{} : {}".format("avg", total/length)
    # print datetime.datetime.now().strftime("%Y,%m,%d")

    if(length==0):
        return

    total -= max;
    length-= 1;

    avg = total/length;
    date = datetime.datetime.now();
    date = date.strftime("%Y,%m,%d,%H,%M");
    date = re.sub(r'(2017,)(\d{1,2})', lambda m: str("2017," + str((int(m.group(2)) - 1))), date.rstrip())

    if(not os.path.isfile(file)):
        w = open(file, 'w')
        w.write('{ \n  "cols": [ \n    {"id":"","label":"Time","pattern":"","type":"date"}, \n    {"id":"","label":"Avg","pattern":"","type":"number"}, \n {"id":"","label":"Max","pattern":"","type":"number"}, \n {"id":"","label":"Min","pattern":"","type":"number"}\n  ],\n  "rows": [')
        w.write(' {"c":[{"v":"Date(')
        w.write(date)

        w.write(')","f":null},{"v":')
        w.write("{}".format(avg))
        w.write(',"f":null}')

        w.write(',{"v":')
        w.write("{}".format(max))
        w.write(',"f":null}')

        w.write(',{"v":')
        w.write("{}".format(min))
        w.write(',"f":null}')
        w.write(']}')

        w.write('\n ]}')
        w.close()
    else:
        readFile = open(file)
        lines = readFile.readlines()
        readFile.close()
        w = open(file, 'w')
        w.writelines([item for item in lines[:-2]])
        w.write(lines[len(lines)-2])
        w.write(",\n")
        w.write(' {"c":[{"v":"Date(')
        w.write(date)

        w.write(')","f":null},{"v":')
        w.write("{}".format(avg))
        w.write(',"f":null}')

        w.write(',{"v":')
        w.write("{}".format(max))
        w.write(',"f":null}')

        w.write(',{"v":')
        w.write("{}".format(min))
        w.write(',"f":null}')
        w.write(']}')

        w.write('\n ]}')
        w.close()
        print(file+" complete")
        time.sleep(1)

    return
# GPUs
getPrices("https://geizhals.de/?cat=gra16_512&xf=9810_7+5783+-+GTX+1070&asuch=&bpmax=&v=e&hloc=at&hloc=de&plz=&dist=&mail=&sort=t&bl1_id=1000","res/gtx_1070.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9809_14+11518+-+Radeon+RX+Vega+64+Liquid","res/amd_rx_V64_W.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9809_14+10215+-+Radeon+RX+Vega+64","res/amd_rx_V64.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9809_13+5792+-+RX+580+(XTX)","res/amd_rx_580.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9809_13+4784+-+RX+570","res/amd_rx_570.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9809_13+2406+-+RX+560","res/amd_rx_560.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9810_7+10609+-+GTX+1080+Ti","res/gtx_1080ti.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9810_7+8228+-+GTX+1080","res/gtx_1080.json")
getPrices("https://geizhals.de/?cat=gra16_512&xf=9810_7+3855+-+GTX+1060+(6GB)","res/gtx_1060.json")
getPrices("https://geizhals.de/?cat=gra16_512&v=e&hloc=at&hloc=de&sort=p&bl1_id=1000&xf=9810_7+3470+-+GTX+1060+(3GB)","res/gtx_1060_3.json")

getPrices("https://geizhals.de/?cat=gra16_512&xf=9810_7+1981+-+GTX+1050+Ti&asuch=&bpmax=&v=e&hloc=at&hloc=de&plz=&dist=&mail=&sort=p&bl1_id=1000","res/gtx_1050Ti.json")
getPrices("https://geizhals.de/?cat=gra16_512&v=e&hloc=at&hloc=de&sort=p&bl1_id=1000&xf=9810_7+1733+-+GTX+1050","res/gtx_1050.json")

# Memory
getPrices("https://geizhals.de/?cat=ramddr3&xf=253_8192~5828_DDR3&asuch=&bpmax=&v=e&hloc=at&hloc=de&plz=&dist=&mail=&sort=t&bl1_id=1000","res/DDR3_8GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_16384%7E5828_DDR3","res/DDR3_16GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_32768%7E5828_DDR3","res/DDR3_32GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_65536%7E5828_DDR3","res/DDR3_64GB.json")

getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_8192%7E5828_DDR4","res/DDR4_8GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_16384%7E5828_DDR4","res/DDR4_16GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_32768%7E5828_DDR4","res/DDR4_32GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_65536%7E5828_DDR4","res/DDR4_64GB.json")
getPrices("https://geizhals.de/?cat=ramddr3&v=e&hloc=at&hloc=de&sort=t&bl1_id=1000&xf=253_131072%7E5828_DDR4","res/DDR4_128GB.json")

# Storage
getPrices("https://geizhals.de/?cat=hde7s&xf=1541_1000%7E958_1000","res/HDD1TB.json")
getPrices("https://geizhals.de/?cat=hde7s&xf=1541_2000%7E958_2000","res/HDD2TB.json")
getPrices("https://geizhals.de/?cat=hde7s&xf=1541_3000%7E958_3000","res/HDD3TB.json")
getPrices("https://geizhals.de/?cat=hde7s&xf=1541_4000%7E958_4000","res/HDD4TB.json")
getPrices("https://geizhals.de/?cat=hde7s&xf=1541_8000%7E958_8000","res/HDD8TB.json")

getPrices("https://geizhals.de/?cat=hdssd&xf=2028_128%7E252_120","res/SSD120.json")
getPrices("https://geizhals.de/?cat=hdssd&xf=2028_256%7E252_240","res/SSD256.json")
getPrices("https://geizhals.de/?cat=hdssd&xf=2028_512%7E252_480","res/SSD512.json")
getPrices("https://geizhals.de/?cat=hdssd&xf=2028_1000%7E252_960","res/SSD1000.json")
getPrices("https://geizhals.de/?cat=hdssd&xf=2028_2000~252_1900","res/SSD2000.json")
